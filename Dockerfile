FROM registry.gitlab.com/gitlab-org/gitlab-omnibus-builder/ubuntu_16.04:0.0.51
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-add-repository ppa:brightbox/ruby-ng
RUN apt-get update
RUN apt-get install -y sudo curl git tzdata ruby2.6 ruby2.6-dev vim build-essential zlib1g-dev

COPY gitlab.deb /root/gitlab.deb
RUN dpkg -i /root/gitlab.deb

#RUN curl -s https://packages.gitlab.com/install/repositories/gitlab/nightly-builds/script.deb.sh | sudo bash
#RUN sudo apt-get install -y gitlab-ce=11.11.0+rnightly.111483.c9e7c48d-0
COPY ./entrypoint /bin/entrypoint
