# Pages package testing

## Getting Started

### Setup folders

Create a local omnibus folder, and copy in the files for the config directory

```
mkdir omnibus
mkdir omnibus/config
```

### Download Debian package

Download the latest GitLab `deb` package from https://packages.gitlab.com/gitlab/gitlab-ee and place it in the config directory. Rename the file to `gitlab.deb`


TODO - not working
```
wget -c https://packages.gitlab.com/gitlab/gitlab-ee/packages/debian/stretch/gitlab-ee_12.5.7-ee.0_amd64.deb -O config/gitlab.deb
```

### Clone the Omnibus repo

```
git clone git@gitlab.com:gitlab-org/omnibus-gitlab.git
```

## Build the environment

```
docker-compose up --build
```

This will also create the following files and folders in the config directory

```
gitlab-secrets.json
trusted-certs
gitlab.rb
```

### Modify gitlab.rb as needed

```
vim omnibus/config/gitlab.rb
```

### Shell into the Docker container AND reconfigure GitLab to create /opt/gitlab 

```
docker-compose exec omnibus bash
```

```
gitlab-ctl reconfigure
```

### View Pages command line params 

```
cat /opt/gitlab/sv/gitlab-pages/run
```

### Tail Pages logs 

```
gitlab-ctl tail gitlab-pages
```

### See GitLab Config 

```
gitlab-ctl show-config
```

### Run Pages Tests 

```
./bin/rspec spec/chef/recipes/gitlab-pages_spec.rb
```

### Shutdown 

```
docker-compose down 
```

